﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    //tiempo inicial
    public int initialTime = 0;

    //escala de tiempo para que cambie de 1 en 1
    private float timeScale = 1;
    
    //texto a mostrar
    private Text clockText;
    
    //Escala de tiempo de cada frame
    private float timeFrameScale = 0f;

    //tiempo en segundos
    private float timeSecondToShow = 0f;

    //escala de tiempo inicial
    private float initialTimeScale;

    // Start is called before the first frame update
    void Start()
    {
        //ponemos el tiempo inicial a 0
        initialTime = 0;

        //asignamos la escala de tiempo
        initialTimeScale = timeScale;
        //seleccionamos el elemento del UI para escribir el tiempo
        clockText = GetComponent<Text>();
        //iniciamos el tiempo inicial a mostrar
        timeSecondToShow = initialTime;

        //actualizamos el temporizador
        UpdateClock(initialTime);

    }

    // Update is called once per frame
    void Update()
    {
        //Se asigna el nuevo tiempo al frame
        timeFrameScale = Time.deltaTime * timeScale;
        //se suma el tiempo al anterior
        timeSecondToShow += timeFrameScale;
        //se actualiza en la UI
        UpdateClock(timeSecondToShow);
    }

    /// <summary>
    /// Metodo que actualiza el tiempo en el reloj de la UI
    /// </summary>
    /// <param name="timeSeconds">tiempo</param>
    public void UpdateClock(float timeSeconds)
    {
        //minutos
        int min = 0;
        //segundos
        int sec = 0;
        //texto a mostrar
        string text;

        //si los segundo son menores que 0
        if (timeSeconds < 0)
        {
            //ponemos el tiempo a 0
            timeSeconds = 0;
        }
        //calculo de los minutos
        min = (int)timeSeconds / 60;
        //calculo de los segundos
        sec = (int)timeSeconds % 60;

        //Se asigna el texto a mostrar en formato reloj(00:00) a la UI
        text = min.ToString("00") + ":" + sec.ToString("00");
        clockText.text = text;
    }
}
