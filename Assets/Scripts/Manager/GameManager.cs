﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace JS
{
    /// <summary>
    /// Script para controlar el sistema de juego
    /// </summary>
    public class GameManager : MonoBehaviour
    {
        //contenedor del jugador actual
        public PlayerHolder currentPlayer;

        //Cartas de los jugadores
        public CardHolders playerHolder;
        //public CardHolders otherPlayerHolder;

        //estado actual
        public State currentState;
        //plantilla de la carta en la interfaz
        public GameObject cardPrefab;

        //indice de turnos
        public int turnIndex;
        //array con los turnos
        public Turn[] turns;
        //eventos del turno
        public SO.GameEvent onTurnChanged;
        public SO.GameEvent onPhaseChanged;
        public SO.StringVariable turnText;

        public static GameManager singleton;

        public Text txt;
        private int points=0;

        public void Awake()
        {              

            singleton = this;
            currentPlayer = turns[0].player;

            //MyCardDownAreaLogic[] mcdla = GameObject.FindObjectsOfType<MyCardDownAreaLogic>();
            //foreach (var item in mcdla)
            //{
            //    item.isActive = true;
            //    Debug.Log("GM " + item.isActive + " " + item.areaName);
            //}            
        }

        
        /// <summary>
        /// Metodo que se ejecuta al inicio
        /// establece el GameManager y el turno inicial
        /// </summary>
        private void Start()
        {
            //FinishGame(points);
            txt.text = "0";

            //MyCardDownAreaLogic[] mcdla = GameObject.FindObjectsOfType<MyCardDownAreaLogic>();
            //foreach (var item in mcdla)
            //{
            //    Debug.Log("GM " + item.isActive + " " + item.areaName);
            //}

            //establece el game manager
            Settings.gameManager = this;
            //if (currentPlayer.cardsDown == null)
            //{
            //    currentPlayer.cardsDown = new List<CardInstance>();
            //}
            //Debug.Log(currentPlayer.handCards.Count());
                   

            currentPlayer.currentHolder = playerHolder;

            

            //pone las cartas iniciales en la mano
            
            currentPlayer.Init();
            currentPlayer.currentHolder.LoadPlayer(currentPlayer);

            //for (int i = 0; i < 4; i++)
            //{
            //    this.PickNewCardFromDeck(currentPlayer);
            //}

            //this.PickNewCardFromDeck(currentPlayer);

            //Settings.RegisterEvent("Cartas creadas para " + currentPlayer.userName, currentPlayer.playerColor);

            turns[0].OnTurnStart();

            //asignar el jugador inicial
            turnText.value = turns[turnIndex].player.userName;
            //establecer el listener de cambio de turno de juego
            onTurnChanged.Raise();

            //MyCardDownAreaLogic[] l = FindObjectsOfType<MyCardDownAreaLogic>();
            //foreach (var item in l)
            //{
            //    Debug.Log("Mycarddown aera logic "+ l.ToString());
            //    item.isActive = true;
            //}
        }

        /// <summary>
        /// metodo para establecer las cartas iniciales
        /// </summary>
        void CreateStartinngCards()
        {
            //obtener el ResourceManager
            ResourcesManager rm = Settings.GetResourcesManager();

            //para cada carta inicial de la mano
            //for (int i = 0; i < currentPlayer.startingCards.Length; i++)
            //{
            //    //se instancia la plantilla(prefab)
            //    GameObject go = Instantiate(cardPrefab) as GameObject;
            //    //se cargan los elementos en la interfaz
            //    CardViz v = go.GetComponent<CardViz>();
            //    v.LoadCard(rm.GetCardInstance(currentPlayer.startingCards[i]));
            //    CardInstance inst = go.GetComponent<CardInstance>();
            //    //se establece el comprotamiento de las cartas en la mano
            //    inst.currentLogic = currentPlayer.handLogic;
            //    //se establecen las posiciones de origen en la mano
            //    Settings.SetParentForCard(go.transform, currentPlayer.currentHolder.handGrid.value);
            //    currentPlayer.handCards.Add(inst);

            //}
            
            playerHolder.LoadPlayer(currentPlayer);

            //Settings.RegisterEvent("Cartas creadas para " + currentPlayer.userName, currentPlayer.playerColor);

        }

        public GameObject finishGame;

        public void PickNewCardFromDeck(PlayerHolder p)
        {
            ResourcesManager rm = Settings.GetResourcesManager();

            if (p.all_cards.Count == 0 && p.handCards.Count==0)
            {
                Settings.RegisterEvent("Juego Finalizado", Color.yellow);               
                return;
            }

            if (p.all_cards.Count>0&&p.handCards.Count<5)
            {
                for (int i = p.handCards.Count; i < 5; i++)
                {
                    string cardId = p.all_cards[0];
                    p.all_cards.RemoveAt(0);
                    //se instancia la plantilla(prefab)
                    GameObject go = Instantiate(cardPrefab) as GameObject;
                    //se cargan los elementos en la interfaz
                    CardViz v = go.GetComponent<CardViz>();
                    v.LoadCard(rm.GetCardInstance(cardId));
                    CardInstance inst = go.GetComponent<CardInstance>();
                    //se establece el comprotamiento de las cartas en la mano
                    inst.currentLogic = p.handLogic;
                    //se establecen las posiciones de origen en la mano
                    Settings.SetParentForCard(go.transform, p.currentHolder.handGrid.value);
                    p.handCards.Add(inst);
                    Settings.RegisterEvent("Robaste " + inst.viz.properties[0].text.text.ToString(), Color.blue);
                    //LoadPlayerOnHolder(p, p.currentHolder); 
                }
            }
            else
            {                
                Settings.RegisterEvent("Solo puedes tener 5 cartas" , Color.red);
            }

        }

        /// <summary>
        /// Metodo que se ejecuta en cada frame del juego mientras el script este activo
        /// controla los turnos y el estado del jugador
        /// </summary>
        private void Update()
        {
            txt.text = points.ToString();
            bool isComplete = turns[turnIndex].Execute();

            //si el turno es completado
            if (isComplete)
            {
                //suma 1
                turnIndex++;
                //si se ha alcanzado el maximo de turnos vuelve a 0
                if (turnIndex >turns.Length-1)
                {
                    turnIndex = 0;
                }

                //Cambio de turno

                currentPlayer = turns[turnIndex].player;
                turns[turnIndex].OnTurnStart();

                //pone en el texto el turno actual
                turnText.value = turns[turnIndex].player.userName;
                onTurnChanged.Raise();

                
            }

            //temporizador del estado del turno
            if (currentState != null)
            {
                currentState.Tick(Time.deltaTime);
            }

            if (currentPlayer.all_cards.Count == 0 && currentPlayer.handCards.Count == 0)
            {
                FinishGame(points);
               
                //GameObject.Find("FinishGame").SetActive(true);                                
                return;
            }

        }

        public void FinishGame(int points)
        {
            finishGame.SetActive(true);
            Text time = GameObject.Find("Time").GetComponent<Text>();
            time.text ="Tiempo: "+ GameObject.Find("Timer").GetComponent<Text>().text;
            Text point = GameObject.Find("Points").GetComponent<Text>();
            point.text = "Puntuación: " + points;//GameObject.Find("Puntuation").GetComponent<Text>().text;
        }

        
        
        public void OnClickYes()
        {            
            if (GameObject.Find("InputField").GetComponent<InputField>().text != "") //&& GameObject.Find("InputField").GetComponent<InputField>().text !=null)
            {
                string time = GameObject.Find("Timer").GetComponent<Text>().text;
                string name = GameObject.Find("InputField").GetComponent<InputField>().text;
                int score = int.Parse(txt.text);
                HighScore.AddHighScoreEntry(score,name,time);

                SceneManager.LoadScene("MainMenu");
            }
        }

        public void OnClickNo()
        {
            SceneManager.LoadScene("MainMenu");
        }
        

        /// <summary>
        /// metodo para establecer el estado del turno
        /// </summary>
        /// <param name="state">estado del turno</param>
        public void SetState(State state)
        {
            currentState = state;
        }

        /// <summary>
        /// metodo para terminar una fase del turno y pasar a la siguiente
        /// </summary>
        public void EndCurrentPhase()
        {
            //Settings.RegisterEvent(turns[turnIndex].name + " ha terminado",currentPlayer.playerColor);
            turns[turnIndex].EndCurrentPhase();
        }

        public void UpdateText(Card c, int cont)
        {           
            int puntuation = int.Parse(txt.text);
            puntuation += (int)((c.properties[1].intValue) * (9+cont));
            points = puntuation;

        }

    }
}
