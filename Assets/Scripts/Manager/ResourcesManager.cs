﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JS
{
    /// <summary>
    /// Script para controlar las cartas del juego
    /// </summary>
    [CreateAssetMenu(menuName = "Managers/Resources Manager")]
    public class ResourcesManager : ScriptableObject
    {
        //tipo de elemento
        public Element typeElement;
        //array con las cartas del juego
        public Card[] allCards;

        //Diccionario con las cartas
        Dictionary<string, Card> cardsDict = new Dictionary<string, Card>();

        /// <summary>
        /// metodo para iniciar las cartas
        /// </summary>
        public void Init()
        {
            cardsDict.Clear();
            for (int i = 0; i < allCards.Length; i++)
            {
                cardsDict.Add(allCards[i].name, allCards[i]);
            }
        }

        /// <summary>
        /// metodo para instanciar las cartas
        /// </summary>
        /// <param name="id">nombre de la carta</param>
        /// <returns>carta</returns>
        public Card GetCardInstance(string id)
        {
            Card originalCard = GetCard(id);
            if (originalCard == null)
            {
                return null;
            }

            Card newInst = Instantiate(originalCard);
            newInst.name = originalCard.name;
            return newInst;
        }

        /// <summary>
        /// metodo para obtener una carta del diccionario
        /// </summary>
        /// <param name="id">nombre de la carta</param>
        /// <returns>carta</returns>
        Card GetCard(string id)
        {
            Card result = null;
            cardsDict.TryGetValue(id, out result);
            return result;
        }
    } 
}
