﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace JS
{
    /// <summary>
    /// Clase para gestionar diferentes elementos del juego como
    /// el controlador del juego, los objetos o sus posiciones iniciales
    /// </summary>
    public static class Settings
    {
        public static GameManager gameManager;

        private static ResourcesManager _resourcesManager;

        private static ConsoleHook _consoleManager;

        public static void RegisterEvent(string e, Color color)
        {
            if (_consoleManager == null)
            {
                _consoleManager = Resources.Load("ConsoleHook") as ConsoleHook;
            }

            _consoleManager.RegisterEvent(e,color);
        }

        /// <summary>
        /// Metodo para obtener el controlador de recursos
        /// </summary>
        /// <returns>gestor de recursos</returns>
        public static ResourcesManager GetResourcesManager()
        {
            //si el ResourcesManager del sistema no existe
            if (_resourcesManager == null)
            {
                //Carga uno y lo inicializa
                _resourcesManager = Resources.Load("ResourcesManager") as ResourcesManager;
                _resourcesManager.Init();
            }

            return _resourcesManager;
        }

        /// <summary>
        /// Obtener los objetos distribuidos en la interfaz
        /// </summary>
        /// <returns>lista de referencias objetos</returns>
        public static List<RaycastResult> GetUIObjs()
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current)
            {
                position = Input.mousePosition
            };

            List<RaycastResult> results = new List<RaycastResult>();

            EventSystem.current.RaycastAll(pointerData, results);
            return results;
        }

        public static void DropCard(Transform c, Transform p, CardInstance cardInst)
        {
            SetParentForCard(c, p);
            gameManager.currentPlayer.DropCard(cardInst);            
            MyCardDownAreaLogic.Increment();            

        }

        /// <summary>
        /// Metodo para controlar las posiciones de origen de las cartas
        /// </summary>
        /// <param name="c">controlador de interfaz del objeto actual</param>
        /// <param name="p">controlador de interfaz del origen("padre")</param>
        public static void SetParentForCard(Transform c, Transform p)
        {
            c.SetParent(p);
           
            Vector3 v = new Vector3((float)0.550073, (float)0.550073, (float)0.550073);           
            c.localPosition = Vector3.zero;
            c.localEulerAngles = Vector3.zero;
            c.localScale = Vector3.Scale(Vector3.one, v);

        }

    } 
}
