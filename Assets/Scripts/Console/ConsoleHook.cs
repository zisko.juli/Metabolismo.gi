﻿using UnityEngine;
using System.Collections;

namespace JS
{
	[CreateAssetMenu(menuName ="Console/Hook")]
	public class ConsoleHook : ScriptableObject
	{
		[System.NonSerialized]
		public ConsoleManager consoleManager;

		public void RegisterEvent(string s, Color color)
		{
			consoleManager.RegisterEevent(s, color);
		}

	}

}