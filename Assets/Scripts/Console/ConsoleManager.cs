﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace JS
{
	public class ConsoleManager : MonoBehaviour
	{
		public Transform consoleGrid;
		public GameObject prefab;
		Text[] txtObjects;
		int index;

		public ConsoleHook hook;

		private void Awake()
		{
			txtObjects = new Text[5];
			hook.consoleManager = this;
			for (int i = 0; i < 5; i++)
			{
				GameObject go = Instantiate(prefab) as GameObject;
				txtObjects[i] = go.GetComponent<Text>();
				go.transform.SetParent(consoleGrid);
			}
		}

		public void RegisterEevent(string s, Color color)
		{
			index++;
			if (index > txtObjects.Length - 1)
			{
				index = 0;
			}

			txtObjects[index].color = color;
			txtObjects[index].text = s;
			txtObjects[index].gameObject.SetActive(true);
		}
	} 
}
