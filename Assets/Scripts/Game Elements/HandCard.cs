﻿using UnityEngine;
using System.Collections;

namespace JS
{
    /// <summary>
    /// Script que especifica el comportamiento logico de las artas en la mano
    /// </summary>
    [CreateAssetMenu(menuName = "Game Elements/Hand Card")]
    public class HandCard : GE_logic
    {
        //evento de seleccion de carta
        public SO.GameEvent onCurrentCardSelected;
        //carta seleccionada
        public CardVariable currentCard;
        //estado de la carta
        public State holdingCard;

        /// <summary>
        /// metodo que define lo que sucede a la carta al clicarla
        /// </summary>
        /// <param name="inst">instancia de la carta</param>
        public override void OnClick(CardInstance inst)
        {
            currentCard.Set(inst);
            Settings.gameManager.SetState(holdingCard);
            onCurrentCardSelected.Raise();
        }

        /// <summary>
        /// metodo para asignar efectos visuales
        /// </summary>
        /// <param name="inst">instancia de la carta</param>
        public override void OnHighlight(CardInstance inst)
        {

        }
    }

}