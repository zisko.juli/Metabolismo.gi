﻿using UnityEngine;
using System.Collections;

namespace JS
{
    /// <summary>
    /// Script que define los metodos de las cartas en juego sobre el tablero
    /// </summary>
    [CreateAssetMenu(menuName = "Game Elements/My Card Down")]
    public class MyCardDown : GE_logic
    {
        /// <summary>
        /// comportamiento para el evento onClick
        /// </summary>
        /// <param name="inst">instancia de la carta</param>
        public override void OnClick(CardInstance inst)
        {
            Debug.Log("Carta colocada en el tablero");
        }

        /// <summary>
        /// metodo para asignar efectos visuales
        /// </summary>
        /// <param name="inst">instancia de la carta</param>
        public override void OnHighlight(CardInstance inst)
        {

        }
    } 
}
