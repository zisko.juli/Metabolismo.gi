﻿using UnityEngine;
using System.Collections;

namespace JS
{
    /// <summary>
    /// Script que define el comportamiento de las areas de juego en el tablero
    /// </summary>
    public class Area : MonoBehaviour
    {
        //logica del area
        public AreaLogic logic;

        /// <summary>
        /// metodo que controla la accion de soltar la carta en el area
        /// </summary>
        public void OnDrop()
        {
            logic.Execute();
            //GameObject.Find(logic.GetInstanceID().ToString()).SetActive(false);
        }       
       
    } 
}
