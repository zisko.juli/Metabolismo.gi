﻿using UnityEngine;
using System.Collections;

namespace JS
{
    /// <summary>
    /// Script que controla los estados de cada turno
    /// </summary>
    [CreateAssetMenu(menuName = "State")]
    public class State : ScriptableObject
    {
        //acciones
        public Action[] actions;

        /// <summary>
        /// Metodo que asigna el tiempo de las acciones
        /// </summary>
        /// <param name="d">tiempo</param>
        public void Tick(float d)
        {
            for (int i = 0; i < actions.Length; i++)
            {
                actions[i].Execute(d);
            }
        }
    } 
}
