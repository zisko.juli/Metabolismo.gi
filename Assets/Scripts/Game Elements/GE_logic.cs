﻿using UnityEngine;
using System.Collections;

namespace JS
{
	/// <summary>
	/// Definicion de la logica de los elementos del juego (cartas)
	/// </summary>
	public abstract class GE_logic : ScriptableObject
	{

		public abstract void OnClick(CardInstance inst);
		public abstract void OnHighlight(CardInstance inst);
	} 
}
