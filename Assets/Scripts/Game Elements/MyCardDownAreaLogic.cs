﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.UI;
using System;

namespace JS
{
    /// <summary>
    /// Script que define el area de juego de las cartas en la mano
    /// </summary>
    [CreateAssetMenu(menuName = "Areas/MyCardDownAreaLogic")]
    public class MyCardDownAreaLogic : AreaLogic
    {
        //carta
        public CardVariable card;
        //tipo de carta
        public CardType cofactorType;
        public CardType productType;
        //variablles de transformacion
        public SO.TransformVariable areaGrid;
        public SO.TransformVariable productGrid;
        //logica que define el comportamiento en esa area
        public GE_logic cardDownLogic;
        
        public string areaName;

        private bool active = true;
        public bool isActive =true;

        public void Start()
        {
            this.active = true;
            this.isActive = this.active;
        }
        

        private static int cont;

        //public MyCardDownAreaLogic(){
        //    Debug.Log("constructor " + this.isActive + " " + this.GetInstanceID().ToString());
        //    this.isActive = true;
        //}

        void Awake()
        {
            this.active = true;
            this.isActive = this.active;            
            cont = 0;
        }

        

        /// <summary>
        /// metodo que controla el comportamiento del area 
        /// </summary>
        public override void Execute()
        {
            
            //isActive = true;
            //Debug.Log("constructor " + this.isActive + " " + this.GetInstanceID().ToString());
            //si la carta es nulo detiene la ejecucion del script
            if (card.value == null)
            {
                return;
            }

            Card c = card.value.viz.card;


            bool canUse = Settings.gameManager.currentPlayer.CanUseCard(c);

            //colocar la carta en la nueva posicion
            if (canUse)
            {
                if (c.name.Equals(areaName) && isActive)
                {
                    //establecer la posicion de origen
                    Settings.DropCard(card.value.transform, areaGrid.value.transform, card.value);
                    //areaName. = "nothing";
                    isActive = false;                    
                    //asignar la logica de las cartas en el tablero
                    card.value.currentLogic = cardDownLogic;
                    //GameObject points = GameObject.Find("Points");                    
                    GameManager.singleton.UpdateText(c,cont);

                }
                else
                {
                    cont = 0;
                    Settings.RegisterEvent("Posición incorrecta" + c.properties[0].stringValue.ToString(), Color.red);
                }

                /*
                //si es de tipo cofactor
                if (c.cardType == cofactorType)
                {
                    bool canUse = Settings.gameManager.currentPlayer.CanUseCard(c);

                    //colocar la carta en la nueva posicion
                    if (canUse)
                    {
                        //if (c.name.Equals(areaName))
                        //{
                            //establecer la posicion de origen
                            Settings.DropCard(card.value.transform, areaGrid.value.transform, card.value);
                            //asignar la logica de las cartas en el tablero
                            card.value.currentLogic = cardDownLogic;
                        //}

                    }

                    //activar la carta para que sea visible
                    card.value.gameObject.SetActive(true);



                }

                //si es de tipo producto
                if (c.cardType == productType)
                {//colocar la carta en la nueva posicion
                    //establecer la posicion de origen
                    Settings.SetParentForCard(card.value.transform, productGrid.value.transform);
                    //activar la carta para que sea visible
                    card.value.gameObject.SetActive(true);
                    //asignar la logica de las cartas en el tablero
                    card.value.currentLogic = cardDownLogic;

                }
                */

            }
            else
            {
                cont = 0;
            }
        }

        
        public static void Increment()
        {
            cont++;
        }

    }
}
