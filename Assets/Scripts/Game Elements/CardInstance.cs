﻿using UnityEngine;
using System.Collections;

namespace JS
{
    /// <summary>
    /// Script que asigna el comportamiento de la interfaz clicable
    /// </summary>
    public class CardInstance : MonoBehaviour, IClicable
    {       
        
        //elementos visuales
        public CardViz viz;

        //logica del objeto
        public GE_logic currentLogic;

        /// <summary>
        /// metodo ejecutado al activarse el script
        /// </summary>
        public void Start()
        {
            this.viz = GetComponent<CardViz>();
            //DontDestroyOnLoad(this.viz);
        }

        //public void Awake()
        //{
        //    this.viz = GetComponent<CardViz>();
        //    DontDestroyOnLoad(this.viz);
        //}

        //public void onDestroy()
        //{
        //    viz = null;
        //}

       

        /// <summary>
        /// metodo que define el evento onClick asignando un comportamiento logico
        /// </summary>
        public void OnClick()
        {
            if (currentLogic == null)
            {
                return;
            }
            currentLogic.OnClick(this);
        }


        /// <summary>
        /// metodo que define los elementos de visualizacion del objeto
        /// </summary>
        public void OnHighlight()
        {
            
            if (currentLogic == null)
            {
                return;
            }
            
            currentLogic.OnHighlight(this);
        }
    }

}
