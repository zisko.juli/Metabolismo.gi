﻿using UnityEngine;
using System.Collections;

namespace JS
{
    /// <summary>
    /// Definicion de las areas logicas del juego para asignar comportamientos
    /// </summary>
    public abstract class AreaLogic : ScriptableObject
    {       

        public abstract void Execute();
     
    } 
}
