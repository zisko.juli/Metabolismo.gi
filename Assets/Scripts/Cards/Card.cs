﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JS
{
    /// <summary>
    /// Clase que contiene los atributos de las cartas
    /// Incluida en el menu de unity para la creación del objeto
    /// </summary>
    [CreateAssetMenu(menuName = "Card")]
    public class Card : ScriptableObject
    {
        //tipo de carta
        public CardType cardType;
        // Resto de propiedades de la carta, 
        // se deja como array por si es necesario añadir diferentes propiedades segun el tipo de carta
        public CardProperties[] properties;
    }

}