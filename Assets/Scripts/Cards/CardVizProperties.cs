﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace JS
{
    /// <summary>
    /// Clase que define las diferentes propiedades del CardViz
    /// </summary>
    [System.Serializable]
    public class CardVizProperties
    {
        public Text text;
        public Image img;
        public Element element;

    } 
}
