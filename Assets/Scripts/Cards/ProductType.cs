﻿using UnityEngine;
using System.Collections;

namespace JS
{
    /// <summary>
    /// Clase que especifica el tipo de carta Cofactor
    /// </summary>
    [CreateAssetMenu(menuName = "Card Type/Product")]
    public class ProductType : CardType
    {
        /// <summary>
        /// metodo para asignar el tipo de carta en la interfaz
        /// </summary>
        /// <param name="viz">clase de visualizacion en la interfaz</param>
        public override void OnSetType(CardViz viz)
        {
            base.OnSetType(viz);

            //ocular dichas propiedades de la carta para este tipo
            //viz.statsHolder.SetActive(false);
            //viz.pointsHolder.SetActive(false);
            
        }

    } 
}
