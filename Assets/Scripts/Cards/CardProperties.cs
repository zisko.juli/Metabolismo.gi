﻿using UnityEngine;
using System.Collections;

namespace JS
{
    /// <summary>
    /// Clase que define los atributos relativos a las propiedades de las cartas
    /// 
    /// </summary>
    [System.Serializable]
    public class CardProperties
    {
        //atributos de string
        public string stringValue;
        //atributos tipo entero
        public int intValue;
        //atributos tipo imagen
        public Sprite sprite;
        //atributos tipo elemento
        public Element element;

    } 
}
