﻿using UnityEngine;
using System.Collections;

namespace JS
{
	/// <summary>
	/// Script que identifica un elemento de tipo entero en la interfaz
	/// </summary>
	[CreateAssetMenu(menuName = "Elements/Int")]
	public class ElementInt : Element
	{


	} 
}
