﻿using UnityEngine;
using System.Collections;

namespace JS
{
    /// <summary>
    /// Clase que especifica el tipo de carta Cofactor
    /// </summary>
    [CreateAssetMenu(menuName = "Card Type/Cofactor")]
    public class CofactorType : CardType
    {
        /// <summary>
        /// metodo para asignar el tipo de carta en la interfaz
        /// </summary>
        /// <param name="viz">clase de visualizacion en la interfaz</param>
        public override void OnSetType(CardViz viz)
        {
            base.OnSetType(viz);
            //viz.pointsHolder.SetActive(true);
            //viz.statsHolder.SetActive(true);
        }

    } 
}
