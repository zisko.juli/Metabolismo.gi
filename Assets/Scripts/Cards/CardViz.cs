﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace JS
{
    /// <summary>
    /// Clase que muestra los atributos de la carta en la interfaz
    /// al ser MonoBehaviour tendrá asociado un objeto de la interfaz
    /// </summary>
    public class CardViz : MonoBehaviour
    {
        //Atributo carta
        public Card card;
        //Atributo de propiedades de visualizacion
        public CardVizProperties[] properties;
        //atributo de estadisticas
        public GameObject statsHolder;
        //atributo para los puntos de la carta
        public GameObject pointsHolder;

       /// <summary>
       /// Metodo que carga la carta, muestra los valores en la interfaz
       /// </summary>
       /// <param name="c">carta</param>
        public void LoadCard(Card c)
        {
            //si la carta es nula deja de ejecutar el metodo
            if (c == null)
            {
                return;
            }
            
            //asigna la carta
            card = c;
            //establece el tipo
            c.cardType.OnSetType(this);

            //oculta todas las propiedades de la interfaz
            CloseAll();

            //recorre las propiedades y va asignando los valores a la carta 
            //y muestra aquellos campos que contengan un valor
            for (int i = 0; i < c.properties.Length; i++)
            {
                CardProperties cp = c.properties[i];
                CardVizProperties p = GetProperty(cp.element);

                if (p == null)
                {
                    continue;
                }

                if (cp.element is ElementText)
                {
                    p.text.text = cp.stringValue;
                    p.text.gameObject.SetActive(true);

                }
                else if (cp.element is ElementInt)
                {
                    p.text.text = cp.intValue.ToString();
                    p.text.gameObject.SetActive(true);

                }
                else if (cp.element is ElementImage)
                {
                    p.img.sprite = cp.sprite;
                    p.img.gameObject.SetActive(true);
                }
            }

        }

        /// <summary>
        /// Metodo para ocultar todos los campos 
        /// </summary>
        public void CloseAll()
        {
            foreach (CardVizProperties p in properties)
            {
                if(p.img != null)
                {
                    p.img.gameObject.SetActive(false);
                }
                if (p.text != null)
                {
                    p.text.gameObject.SetActive(false);
                }

            }
        }

        /// <summary>
        /// metodo para obtener el elemento del array de propiedades
        /// </summary>
        /// <param name="e">elemento</param>
        /// <returns></returns>
        public CardVizProperties GetProperty(Element e)
        {
            CardVizProperties result = null;
            for (int i = 0; i < properties.Length; i++)
            {
                if (properties[i].element == e)
                {
                    result = properties[i];
                    break;
                }
            }
            return result;

        }
    } 
}
