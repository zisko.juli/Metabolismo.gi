﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JS
{
	/// <summary>
	/// Clase que define el tipo de carta
	/// </summary>
	public abstract class CardType : ScriptableObject
	{
		// atributo para el nombre del tipo
		public string TypeName;
		/// <summary>
		/// metodo para establecer el valor del tipo en la clase de visualizacion de las propiedades de las cartas
		/// </summary>
		/// <param name="viz">clase que muestra las propiedades de las cartas en la interfaz</param>
		public virtual void OnSetType(CardViz viz)
		{
			//obtener el elemento correspondiente al tipo de carta
			Element t = Settings.GetResourcesManager().typeElement;
			// obtener el valor correspondiente al tipo de carta
			CardVizProperties type = viz.GetProperty(t);
			// asignar el valor para mostrarlo en la interfaz
			type.text.text = TypeName;
		}
	} 
}
