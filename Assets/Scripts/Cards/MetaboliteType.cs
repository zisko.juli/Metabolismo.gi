﻿using UnityEngine;
using System.Collections;

namespace JS
{
    /// <summary>
    /// Clase que especifica el tipo de carta Coenzima
    /// </summary>
    [CreateAssetMenu(menuName = "Card Type/Intermediario")]
    public class MetaboliteType : CardType
    {
        /// <summary>
        /// metodo para asignar el tipo de carta en la interfaz
        /// </summary>
        /// <param name="viz">clase de visualizacion en la interfaz</param>
        public override void OnSetType(CardViz viz)
        {
            base.OnSetType(viz);
            //viz.pointsHolder.SetActive(false);
            //viz.statsHolder.SetActive(false);
        }

    } 
}
