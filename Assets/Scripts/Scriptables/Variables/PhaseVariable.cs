﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JS
{
    /// <summary>
    /// Variable para manejar las fases de los turnos
    /// </summary>
    [CreateAssetMenu(menuName ="Variables/Phase")]
    public class PhaseVariable : ScriptableObject
    {
        public Phase value;
    } 
}
