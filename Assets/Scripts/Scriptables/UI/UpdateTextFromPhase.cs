﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SA;
using SO.UI;

namespace JS
{
    //Clase que actualiza el texto de la interfaz correspondiente a la fase
    public class UpdateTextFromPhase : UIPropertyUpdater
    {
        public PhaseVariable currentPhase;
        public Text targetText;
        private int puntuation;

        /// <summary>
        /// Copia de: UpdateText
        /// </summary>
        public override void Raise()
        {
            targetText.text = currentPhase.value.phaseName;
        }        
    } 
}


