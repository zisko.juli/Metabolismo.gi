﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace JS
{
	/// <summary>
	/// Script que controla la accion de soltar la carta con el raton
	/// </summary>
	[CreateAssetMenu(menuName = "Actions/MouseHoldWithCard")]
	public class MouseHoldWithCard : Action
	{
		//carta actual
		public CardVariable currentCard;
		//Estado del jugador
		public State playerControlState;
		//evento de control del jugador
		public SO.GameEvent onPlayerControlState;

		/// <summary>
		/// Metodo que controla la accion de soltar el raton
		/// </summary>
		/// <param name="d"></param>
		public override void Execute(float d)
		{
			bool mouseIsDown = Input.GetMouseButton(0);
			//si se suelta el clic del raton
			if (!mouseIsDown)
			{
				//obtener los objetos de la interfaz
				List<RaycastResult> results = Settings.GetUIObjs();

				//por cada objeto
				foreach (RaycastResult r in results)
				{
					//comprobar las areas disponibles para soltar la carta
					Area a = r.gameObject.GetComponentInParent<Area>();
					
					//si existe el area
					if (a != null)
					{
						//añade la carta en esa zona
						a.OnDrop();
						//GameObject.FindGameObjectWithTag(a.name.ToString()).SetActive(false);
						break;
					}
				}				

				//muestra de nuevo la carta seleccionada
				currentCard.value.gameObject.SetActive(true);

				//establece a nulo la carta seleccionada
				currentCard.value = null;

				//cambia el estado del jugador
				Settings.gameManager.SetState(playerControlState);
				onPlayerControlState.Raise();
				return;
			}
		}
	}
}
