﻿using UnityEngine;
using System.Collections;

namespace JS
{
	/// <summary>
	/// Clase para definir las acciones en el juego
	/// </summary>
	public abstract class Action : ScriptableObject
	{
		public abstract void Execute(float d);

	} 
}
