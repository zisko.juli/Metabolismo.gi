﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace JS
{
    /// <summary>
    /// Script que define el comportamiento del clic del ratón
    /// </summary>
    [CreateAssetMenu(menuName = "Actions/OnMouseClick")]
    public class OnMouseClick : Action
    {
        /// <summary>
        /// metodo especifica el comportamiento
        /// </summary>
        /// <param name="d">variable de tiempo</param>
        public override void Execute(float d)
        {
            //si el boton izquierdo esta pulsado
            if (Input.GetMouseButtonDown(0))
            {
                //recoge las posiciones de los objetos
                List<RaycastResult> results = Settings.GetUIObjs();

                //asigna la interfaz clicable a cada objeto
                foreach (RaycastResult r in results)
                {
                    IClicable c = r.gameObject.GetComponentInParent<IClicable>();

                    if (c != null)
                    {
                        c.OnClick();
                        break;
                    }
                }
            }

        }
    }

}