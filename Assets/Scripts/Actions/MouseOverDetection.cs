﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace JS
{
    /// <summary>
    /// Script que controla el paso del cursor por una carta
    /// </summary>
    [CreateAssetMenu(menuName = "Actions/MouseOverDetection")]
    public class MouseOverDetection : Action
    {
        /// <summary>
        /// Metodo que controla el paso del cursor por un objeto
        /// </summary>
        /// <param name="d">variable de tiempo</param>
        public override void Execute(float d)
        {           
            //obtener los objetos de la interfaz
            List<RaycastResult> results = Settings.GetUIObjs();
                        
            IClicable c = null;

            //para cada objeto se asigna la interfaz clicable
            foreach (RaycastResult r in results)
            {                
                c = r.gameObject.GetComponentInParent<IClicable>();

                if (c != null)
                {
                    c.OnHighlight();
                    break;
                }
            }
        }

    } 
}

