﻿using UnityEngine;
using System.Collections;

namespace JS
{
    /// <summary>
    /// Fase de juego
    /// </summary>
    [CreateAssetMenu(menuName = "Turns/Phase2 Player")]
    public class PlayPhase : Phase
    {
        /// <summary>
        /// metodo para determinar si ha terminado la fase
        /// </summary>
        /// <returns>si ha terminado</returns>
        public override bool IsComplete()
        {
            if (forceExit)
            {
                forceExit = false;
                return true;
            }

            return false;
        }

        /// <summary>
        /// metodo que se ejecuta cuando termina la fase
        /// </summary>
        public override void OnEndPhase()
        {
            //si ha empezado la fase
            if (isInit)
            {
                //cambiar el estado del controlador del juego
                Settings.gameManager.SetState(null);
                isInit = false;
            }
        }

        /// <summary>
        /// metodo ejecutado al inicio de la fase
        /// </summary>
        public override void OnStartPhase()
        {
            //si la fase no ha empezado
            if (!isInit)
            {
                //Debug.Log(this.name + " inicio fase");

                //asigna el estado al jugador
                Settings.gameManager.SetState(null);
                //activa el evento de cambio de fase
                Settings.gameManager.onPhaseChanged.Raise();
                isInit = true;
            }
        }
    }
}
