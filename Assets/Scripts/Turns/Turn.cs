﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JS
{
	/// <summary>
	/// Script que define el turno del juegador
	/// </summary>
	[CreateAssetMenu(menuName ="Turns/Turn")]
	public class Turn : ScriptableObject
	{
		//jugador
		public PlayerHolder player;
		//indice de turnos
		[System.NonSerialized]
		public int index = 0;
		//fase actual del juegador
		public PhaseVariable currentPhase;
		//fases del turno
		public Phase[] phases;

		public PlayerAction[] turnStartActions;

		public void OnTurnStart()
		{
			if(turnStartActions == null)
			{
				return;
			}

			for (int i = 0; i < turnStartActions.Length; i++)
			{
				turnStartActions[i].Execute(player);
			}
		}
		 
		/// <summary>
		/// metodo que controla los turnos
		/// </summary>
		/// <returns>si fase completada</returns>
		public bool Execute()
		{
			bool result= false;
			//fase actual
			currentPhase.value = phases[index];
			//inicio de la fase
			phases[index].OnStartPhase();

			bool phaseIsComplete = phases[index].IsComplete();
			//si la fase ha terminado
			if (phaseIsComplete)
			{

				phases[index].OnEndPhase();
				//Debug.Log(this.name + " final de fase");
				
				//pasar a la fase sigueinte aumentando el indice indice
				index++;
				
				//si ya no hay mas fases reiniciar el indice
				if (index > phases.Length - 1)
				{
					index = 0;
					result = true;
				}
			}
			return result;
		}

		/// <summary>
		/// metodo para indicar que la fase ha terminado
		/// </summary>
		public void EndCurrentPhase()
		{
			phases[index].forceExit = true;
		}
	} 
}
