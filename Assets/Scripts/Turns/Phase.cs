﻿using UnityEngine;
using System.Collections;

namespace JS
{
    /// <summary>
    /// Clase que define los atributos de las fases
    /// </summary>
    public abstract class Phase : ScriptableObject
    {
        //nombre de la fse
        public string phaseName;
        public bool forceExit;

        //si ha terminado
        public abstract bool IsComplete();
        
        //si ha empezado la fase
        [System.NonSerialized]
        protected bool isInit;

        //metodos de inicio y final de fase
        public abstract void OnStartPhase();
        public abstract void OnEndPhase();
    } 
}
