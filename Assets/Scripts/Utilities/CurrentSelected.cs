﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JS
{
    /// <summary>
    /// Script que define el comportamiento de la carta seleccionada 
    /// </summary>
    public class CurrentSelected : MonoBehaviour
    {
        public CardVariable currentCard;

        public CardViz cardViz;

        Transform mTransform;

        /// <summary>
        /// metodo que carga la carta, muestra los valores en la interfaz
        /// </summary>
        public void LoadCard()
        {
            if (currentCard.value == null)
            {
                return;
            }
            //oculta la carta seleccionada actualmente
            currentCard.value.gameObject.SetActive(false);
            //muestra "la carta" correspondiente a la "selección" del raton 
            cardViz.LoadCard(currentCard.value.viz.card);
            cardViz.gameObject.SetActive(true);

        }

        /// <summary>
        /// metodo para ocultar la carta
        /// </summary>
        public void CloseCard()
        {
            cardViz.gameObject.SetActive(false);
        }

        /// <summary>
        /// metodo que se ejecuta al iniciar el script
        /// </summary>
        private void Start()
        {
            mTransform = this.transform;
            CloseCard();
            
        }

        /// <summary>
        /// metodo ejecutado mientras este activo el script
        /// controla la posicion de la carta en la pantalla en función de la posicion del raton
        /// </summary>
        void Update()
        {
            mTransform.position = new Vector3(Input.mousePosition.x-320, Input.mousePosition.y-170, Input.mousePosition.z);
        }
    }

}