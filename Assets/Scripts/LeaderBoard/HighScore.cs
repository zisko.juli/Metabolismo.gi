﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

namespace JS
{
    public class HighScore : MonoBehaviour
    {
        //private Transform entryContainer;
        //private Transform entryTemplate;

        private GameObject entryContainer;
        private GameObject entryTemplate;
        private List<HighScoreEntry> highScoreEntryList;
        private List<GameObject> highScoreList;

        private void Start()
        {
            if (!File.Exists("highscores"))
            {
                File.Create("highscores");
                //Debug.Log("creado: "+File.Exists("highscores"));
            }
        }

        private void Awake()
        {
            //entryContainer = transform.Find("HighScoreContainer");
            //entryTemplate = transform.Find("HighScoreTemplate");
            entryContainer = GameObject.Find("HighScoreContainer");
            entryTemplate = GameObject.Find("HighScoreTemplate");


            entryTemplate.gameObject.SetActive(false);

            //PlayerPrefs.DeleteAll();
            //highScoreEntryList = new List<HighScoreEntry>() {
            //    new HighScoreEntry{score= 12345667,name="AAA",time="23895"},
            //    new HighScoreEntry{score= 86904,name="BBB",time="11111"},
            //    new HighScoreEntry{score= 3456,name="CCC",time="22222"},
            //    new HighScoreEntry{score= 93284,name="DDD",time="33333"},
            //};

            /*
            HighScores highScores = new HighScores { highScoreEntryList = highScoreEntryList };
            string json = JsonUtility.ToJson(highScores);
            PlayerPrefs.SetString("highscoreTable", json);
            PlayerPrefs.Save();
            Debug.Log(PlayerPrefs.GetString("highscoreTable"));
            */


            if (!File.Exists("highscores"))
            {
                var stream = new System.IO.FileStream("highscores", System.IO.FileMode.Create);
                stream.Close();
                //Debug.Log("creado: "+File.Exists("highscores"));
            }


            //AddHighScoreEntry(110, "random", "120");

            //string jsonString = PlayerPrefs.GetString("highscoreTable");
            ///HighScores highScores = JsonUtility.FromJson<HighScores>(jsonString);
            ///JsonSerializer.Deserialize<HighScores>(jsonString)
            string jsonString = File.ReadAllText("highscores");
            //Debug.Log("fichero: "+jsonString);
            //HighScores highScores = JsonConvert.DeserializeObject<HighScores>(jsonString);            
            //HighScores highScores = new HighScores();
            HighScores highScores = JsonUtility.FromJson<HighScores>(jsonString);
            if (highScores == null)
            {
                highScores = new HighScores();
            }
            //ordenar la lista
            for (int i = 0; i < highScores.highScoreEntryList.Count; i++)
            {
                for (int j = 0; j < highScores.highScoreEntryList.Count; j++)
                {
                    if (highScores.highScoreEntryList[j].score < highScores.highScoreEntryList[i].score)
                    {
                        HighScoreEntry aux = highScores.highScoreEntryList[i];
                        highScores.highScoreEntryList[i] = highScores.highScoreEntryList[j];
                        highScores.highScoreEntryList[j] = aux;
                    }
                }
            }

            highScoreList = new List<GameObject>();

            foreach (HighScoreEntry highScoreEntry in highScores.highScoreEntryList)
            {
                CreateHighScoreEntry(highScoreEntry, entryContainer, highScoreList);
            }

            
    
        ////float templateHeight = 30f;

            //for (int i = 0; i < 10; i++)
            //{
            //    //Transform entryTransform =Instantiate(entryTemplate, entryContainer);
            //    //GameObject entry = Instantiate(entryTemplate);
            //    GameObject newGo = Instantiate(entryTemplate);
            //    newGo.transform.parent = entryContainer.transform;
            //    newGo.SetActive(true);
            //    //GameObject entry = Instantiate(entryTemplate);
            //    //RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();

            //    //entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * i);
            //    //entryTransform.gameObject.SetActive(true);

            //    int rank = i + 1;
            //    string rankstring = rank + "º";


            //    int score = Random.Range(0, 1000000);
            //    int t = Random.Range(500, 6000);

            //    newGo.transform.Find("PosTxt").GetComponent<Text>().text = rankstring;

            //    string name = "AAA";
            //    newGo.transform.Find("NamTxt").GetComponent<Text>().text = name;
            //    newGo.transform.Find("PoTxt").GetComponent<Text>().text = score.ToString();
            //    newGo.transform.Find("TiTxt").GetComponent<Text>().text = t.ToString();

        }

        public static void AddHighScoreEntry(int score, string name, string time)
        {
            //crear la puntuacion
            HighScoreEntry highScoreEntry = new HighScoreEntry { score = score, name = name, time = time };

            //cargar las puntuaciones mas altas
            //string jsonString = PlayerPrefs.GetString("highscoreTable");
            //HighScores highScores = JsonUtility.FromJson<HighScores>(jsonString);
            
            if (!File.Exists("highscores"))
            {
                var stream = new System.IO.FileStream("highscores", System.IO.FileMode.Create);
                stream.Close();                
                //Debug.Log("creado: "+File.Exists("highscores"));
            }
            

            string jsonString = File.ReadAllText("highscores");
            //HighScores highScores = new HighScores();            
            HighScores highScores = JsonUtility.FromJson<HighScores>(jsonString);
            if (highScores == null)
            {
                highScores = new HighScores();
            }
            //HighScores highScores = new HighScores();
            //HighScores highScores = JsonSerializer.Deserialize<HighScores>(jsonString);
            //HighScores highScores = JsonConvert.DeserializeObject<HighScores>(highScores);

            //HighScores highScores = JsonConvert.DeserializeObject<HighScores>(jsonString);

            //añadir la nueva puntuacion
            highScores.highScoreEntryList.Add(highScoreEntry);

            //guardar la actualizacion
            //string json = JsonUtility.ToJson(highScores);
            //PlayerPrefs.SetString("highscoreTable", json);
            //PlayerPrefs.Save();
            ///
            //JsonSerializer serializer = new JsonSerializer();
            //serializer.Converters.Add(new JavaScriptDateTimeConverter());
            //serializer.NullValueHandling = NullValueHandling.Ignore;
            //using (StreamWriter sw = new StreamWriter("highscores"))
            //using (JsonWriter writer = new JsonTextWriter(sw))
            //{
            //    serializer.Serialize(writer, highScores);
            //    //jsonString = JsonSerializer.Serialize(highScores);
            //    //File.WriteAllText("highscores", jsonString);
            //}

            //jsonString = JsonConvert.SerializeObject(highScores);
            string json = JsonUtility.ToJson(highScores);
            
            File.WriteAllText("highscores", json);
        }

        private void CreateHighScoreEntry(HighScoreEntry highScoreEntry, GameObject container, List<GameObject> scoreList)
        {
            GameObject entry = Instantiate(entryTemplate);
            //entry.transform.parent = entryContainer.transform;
            entry.transform.SetParent(entryContainer.transform);
            entry.SetActive(true);

            int rank = scoreList.Count + 1;
            string rankstring = rank + "º";


            int score = highScoreEntry.score;
            int t = Random.Range(500, 6000);

            entry.transform.Find("PosTxt").GetComponent<Text>().text = rankstring;

            string name = highScoreEntry.name;
            entry.transform.Find("NamTxt").GetComponent<Text>().text = name;
            entry.transform.Find("PoTxt").GetComponent<Text>().text = score.ToString();
            entry.transform.Find("TiTxt").GetComponent<Text>().text = highScoreEntry.time;

            scoreList.Add(entry);

        }

        private class HighScores
        {
            public List<HighScoreEntry> highScoreEntryList;
            public HighScores()
            {
                highScoreEntryList = new List<HighScoreEntry>();
            }

        }

        [System.Serializable]
        private class HighScoreEntry
        {
            public int score;
            public string name;
            public string time;
        }

    }

    




} 
	
