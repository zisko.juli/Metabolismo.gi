﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace JS
{
    /// <summary>
    /// Clase para establecer los "contenedores" o zonas que van a mostrar las cartas
    /// </summary>
    [CreateAssetMenu(menuName = "Holders/Card Holder")]
    public class CardHolders : ScriptableObject
    {
        //posiciones de las cartas
        public SO.TransformVariable handGrid;
        public SO.TransformVariable downGrid;
        public SO.TransformVariable productGrid;
               
        

        /// <summary>
        /// metodo para cargar las cartas de los jugadores
        /// </summary>
        /// <param name="p">contenedor o holder del jugador</param>
        public void LoadPlayer(PlayerHolder p)        {

            p.handCards = new List<CardInstance>();
            p.cardsDown = new List<CardInstance>();
            foreach (CardInstance c in p.cardsDown)
            {                
                c.viz.gameObject.transform.SetParent(downGrid.value.transform);
            }

            foreach (CardInstance c in p.cardsDown)
            {
                c.viz.gameObject.transform.SetParent(downGrid.value.transform);
            }

            foreach (CardInstance c in p.handCards)
            {                
                                
                c.viz.gameObject.transform.SetParent(downGrid.value.transform);
            }

            //foreach (CardInstance c in p.productCards)
            //{
            //    c.viz.gameObject.transform.SetParent(downGrid.value.transform);
            //}
        }
    } 
}
