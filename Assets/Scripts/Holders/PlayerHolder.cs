﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace JS
{
	/// <summary>
	/// Script que establece los atributos del "contenedor" o holder del jugador
	/// </summary>
	[CreateAssetMenu(menuName ="Holders/Player Holder")]
	public class PlayerHolder : ScriptableObject
	{
		//nombre de usuario
		public string userName;

		public Color playerColor;

		//cartas iniciales
		//public string[] startingCards;
		public List<string> startingDeck = new List<string>();
		[System.NonSerialized]
		public List<string> all_cards = new List<string>();
		

		public GE_logic handLogic;
		public GE_logic downLogic;

		[System.NonSerialized]
		public CardHolders currentHolder;

		public GameObject cardPrefab;
		
		[System.NonSerialized]
		public List<CardInstance> handCards = new List<CardInstance>();
		[System.NonSerialized]
		public List<CardInstance> cardsDown = new List<CardInstance>();
		//[System.NonSerialized]
		//public List<CardInstance> productCards = new List<CardInstance>();

		//public void Awake()
		//{
		//	handCards = new List<CardInstance>();
		//	DontDestroyOnLoad(currentHolder);
		//}

		//public void Start()
		//{
		//	DontDestroyOnLoad(currentHolder);
		//}

		public void Init()
		{
			//all_cards.AddRange(startingDeck);
			all_cards = startingDeck.OrderBy(x => Random.value).ToList();
			
		}		

		public bool CanUseCard(Card c)
		{
			bool result = true;

			return result;
		}

		public void DropCard(CardInstance inst)
		{

			if (cardsDown.Count() > 0)
			{
				//Debug.Log("player holder card DOwn " + cardsDown.Count().ToString());
			}

			if (handCards.Contains(inst))
			{
				handCards.Remove(inst);
			}

			cardsDown.Add(inst);
			
			Settings.RegisterEvent(userName + " ha usado " + inst.viz.card.properties[0].stringValue.ToString(), Color.white);
		}
	} 
}
