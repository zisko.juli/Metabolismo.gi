﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JS
{
    /// <summary>
    /// Interfaz para controlar el clic del raton
    /// </summary>
    public interface IClicable
    {
        void OnClick();
        void OnHighlight();

    } 
}
