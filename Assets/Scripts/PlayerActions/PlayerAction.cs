﻿using UnityEngine;
using System.Collections;

namespace JS
{	
	public abstract class PlayerAction : ScriptableObject
	{
		public abstract void Execute(PlayerHolder player);
	} 
}
